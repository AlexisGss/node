# Installation
Clonez le projet
```
git clone https://gitlab.com/AlexisGss/node.git
```
Déplacez-vous dans le répertoire du projet
```
cd node
```
Installez les dépendances
```
npm install
```
Lancez l'application
```
npm start
```
Accédez à l'application
```
http://localhost:3000/documentation
```

# Variables d'environnement
Le fichier .env va vous permettre de faire fonctionner le service de mail, modifiez donc les variables suivantes :
```
MAIL_USER=votre adresse ethereal
MAIL_PASS=votre mot de passe ethereal
```

# Les favoris
Lors d'un ajout d'un ou de plusieurs films aux favoris, remplacez le mot 'string' par l'adresse mail du compte utilisé :
```
"mail": "string"             /*par défaut*/ 
"mail": "jdeo@gmail.com"     /*après modification*/ 
 ```