'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class Films extends Model {

    static get tableName() {

        return 'films';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            title: Joi.string().min(1).example('Tenet').description('Name of the film'),
            description: Joi.string().min(3).example('Tenet').description('Description of the film'),
            releaseDate: Joi.date().min(3).example('2020-08-12 00:00:00').description('Description of the film'),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            director: Joi.string().min(3).example('Christopher Nolan').description('Director of the film'),
        });
    }

    $beforeInsert() {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate() {
        this.updatedAt = new Date();
    }

};