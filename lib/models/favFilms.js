'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class FavFilms extends Model {

    static get tableName() {

        return 'favFilms';
    }

    static get joiSchema() {

        return Joi.object({
            idUser: Joi.number(),
            idFilm: Joi.number(),
        });
    }

};