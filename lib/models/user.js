'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');
var sha1 = require('sha1');

module.exports = class User extends Model {

    static get tableName() {

        return 'user';
    }

    static get jsonAttributes(){

        return ['scope']
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
            lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            username: Joi.string().min(3).example('JDoe').description('Username of the user'),
            password: Joi.string().min(8).description('Password of the user, at least 8 characters'),
            mail: Joi.string().email().max(256).example('jdoe@gmail.com').description('Email of the user'),
            scope: Joi.string().example('user').description('Role of the user'),
        });
    }

    $beforeInsert() {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
        this.password = sha1(this.password);
        this.scope = 'user';
    }

    $beforeUpdate() {
        this.updatedAt = new Date();
        this.password = sha1(this.password);
    }

};