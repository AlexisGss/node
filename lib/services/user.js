'use strict';

const { Service } = require('schmervice');
const Boom = require("@hapi/boom");
var sha1 = require('sha1');
const Jwt = require('@hapi/jwt');

module.exports = class UserService extends Service {

        async create(user){
                const { User } = this.server.models();

                const { mailService } =  this.server.services();
                mailService.mailerSend(user.mail, "Bienvenue", "Bienvenue dans le projet node " + user.firstName + " " + user.lastName);

                const mail = await User.query().findOne("mail", user.mail).select("mail")
                if(!mail){
                        return User.query().insertAndFetch(user);
                }
                else{
                        return Boom.notAcceptable('Email déjà existant');
                }
        }
        async list(user){
                const { User } = this.server.models();
    
                return User.query().returning(user);
        }
        async delete(user){
                const { User } = this.server.models();
    
                return User.query().deleteById(user.id);
        }
        async update(user){
                const { User } = this.server.models();
    
                return User.query().patchAndFetchById(user.id, user);
        }
        async login(user){
                const { User } = this.server.models();

                const userPassword = await User.query().findOne("mail", user.mail).select('password').then(o=> o.password);
                const userScope = await User.query().findOne("mail", user.mail).select('scope').then(o=> o.scope);
                var decryptPassword = await sha1(user.password)
                
                if(userPassword === decryptPassword){
                        const token = Jwt.token.generate(
                                {
                                        aud: 'urn:audience:iut',
                                        iss: 'urn:issuer:iut',
                                        firstName: 'John',
                                        lastName: 'Doe',
                                        email: 'test@example.com',
                                        scope: userScope,
                                },
                                {
                                        key: 'random_string', // La clé qui est définit dans lib/auth/strategies/jwt.js
                                        algorithm: 'HS512'
                                },
                                {
                                        ttlSec: 14400 // 4 hours
                                }
                        );
                        return [{ login: "successful" }, { key: token}]
                }
                else{
                        var error = new Error('Unauthorized');
                        return Boom.unauthorized('Authentication failed')
                }
        }
}