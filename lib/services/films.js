'use strict';

const { Service } = require('schmervice');
const Boom = require("@hapi/boom");

module.exports = class FilmsService extends Service {

    async create(film){
        const { Films } = this.server.models();

        const { mailService } =  this.server.services();
        const { User } = this.server.models();
        const users = await User.query().select('mail');
        for (let i = 0 ; i < users.length ; i ++) {
            mailService.mailerSend(users[i].mail, "Ajout de film", "Bonjour, le film '" + film.title + "' à été ajouté !");
        }

        const filmTitle = await Films.query().findOne("title", film.title).select('title');
        if (!filmTitle){
            return Films.query().insertAndFetch(film);
        }
        return Boom.notAcceptable('Ce film existe déjà');
    }
    async delete(film){
        const { Films } = this.server.models();

        return Films.query().deleteById(film.id);
    }
    async update(film){
        const { Films } = this.server.models();

        const { mailService } =  this.server.services();
        const { User } = this.server.models();
        const users = await User.query().join('favFilms', 'user.id', '=', 'favFilms.idUser').select('mail').where('favFilms.idFilm', film.id);
        for (let i = 0 ; i < users.length ; i ++) {
            mailService.mailerSend(users[i].mail, "Modification de film", "Bonjour, le film '" + film.title + "', que vous avez en favori, à été modifié !");
        }

        return Films.query().patchAndFetchById(film.id, film);
    }
    async addFav(payload){
        const { FavFilms, Films, User } = this.server.models();
        const idUser = await User.query().findOne("mail", payload.mail).select("id");
        
        if(idUser){
            let error = 0;
            for (const title of payload.title) {
                const idFilm = await Films.query().findOne("title", title).select("id");
                if(idFilm){
                    const fav = await FavFilms.query().findOne({idUser: idUser.id, idFilm: idFilm.id});
                    if(!fav){
                        await FavFilms.query().insert({idUser: idUser.id, idFilm: idFilm.id});
                    }
                    else {
                        error ++;
                    }
                }
            }
            if(error > 0){
                return Boom.notAcceptable("Film déjà en favori");
            }
            return {add: 'successful'};
        }
        else{
            return Boom.notAcceptable('Email invalide');
        }
    }
    async deleteFav(payload){
        const { FavFilms, Films, User } = this.server.models();
        const idUser = await User.query().findOne("mail", payload.mail).select("id");
        
        if(idUser){
            let error = 0;
            for (const title of payload.title) {
                const idFilm = await Films.query().findOne("title", title).select("id");
                if(idFilm){
                    const fav = await FavFilms.query().findOne({idFilm: idFilm.id});
                    if(fav){
                        const verify = await FavFilms.query().findOne({idUser:idUser.id, idFilm:idFilm.id});
                        if(verify){
                            await FavFilms.query().delete().where('idUser', idUser.id).andWhere('idFilm', idFilm.id);
                        }
                    }
                    else {
                        error ++;
                    }
                }
            }
            if(error > 0){
                return Boom.notAcceptable("Film inexistant");
            }
            return {remove: 'successful'};
        }
        else{
            return Boom.notAcceptable('Email invalide');
        }
    }
}