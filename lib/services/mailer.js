'use strict';

const { Service } = require('schmervice');
const nodemailer = require("nodemailer");

module.exports = class mailService extends Service {
    createTransporter() {
        return nodemailer.createTransport({
            host: process.env.MAILER_HOST,
            port: 587,
            auth: {
                user: process.env.MAILER_USER,
                pass: process.env.MAILER_PASSWORD
            }
        });
    }
    mailerSend(mail, title, text){
        const transporter = this.createTransporter();

        const messageInfo = {
          from: process.env.MAILER_FROM,
          to: mail,
          subject: title,
          text: text
        }

        transporter.sendMail(messageInfo, (err, info) => {
          if (err) {
              console.log('Une erreur est survenue. ' + err.messageInfo);
              return process.exit(1);
          }
  
          console.log('Message envoyé: %s', info.messageId);
          console.log('URL de prévisualiation: %s', nodemailer.getTestMessageUrl(info));
      });
    }
}