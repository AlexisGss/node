'use strict';

const Joi = require('joi')

module.exports = {
    method: 'delete',
    path: '/films/fav',
    options: {
        auth : {
            scope: ['user', 'admin']
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                mail: Joi.string().required().email().max(256).example('alexis.gousseau@gmail.com').description('Email of the user'),
                title: Joi.array().required().example(["Tenet", "Interstellar"]).description('Names of films'),
            })
        }
    },
    handler: async (request) => {
        const { filmsService } = request.services();

        return filmsService.deleteFav(request.payload);
    }
};