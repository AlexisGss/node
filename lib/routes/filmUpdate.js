'use strict';

const Joi = require('joi')

module.exports = {
  method: 'patch',
  path: '/films/patch',
  options: {
      auth : {
          scope: [ 'admin' ]
      },
      tags: ['api'],
      validate: {
        payload: Joi.object({
            id: Joi.number().integer().min(1).description("Id of the user"),
            title: Joi.string().required().min(1).example('Tenet').description('Name of the film'),
            description: Joi.string().required().min(3).example('Tenet').description('Description of the film'),
            releaseDate: Joi.date().required().min(3).example('2020-08-12 00:00:00').description('Description of the film'),
            director: Joi.string().required().min(3).example('Christopher Nolan').description('Director of the film'),
        })
      }
  },
  handler: async (request) => {

      const { filmsService } = request.services();

      return await filmsService.update(request.payload);
  }
};