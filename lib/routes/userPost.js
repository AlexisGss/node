'use strict';

const Joi = require('joi')

module.exports = {
  method: 'post',
  path: '/user',
  options: {
      auth: false,
      tags: ['api'],
      validate: {
        payload: Joi.object({
          firstName: Joi.string().required().min(3).example('Alexis').description('Firstname of the user'),
          lastName: Joi.string().required().min(3).example('Gousseau').description('Lastname of the user'),
          username: Joi.string().required().min(3).example('Alex').description('Username of the user'),
          password: Joi.string().required().min(8).description('Password of the user, at least 8 characters'),
          mail: Joi.string().required().email().max(256).example('alexis.gousseau@gmail.com').description('Email of the user'),
        })
      }
  },
  handler: async (request, h) => {
    const { userService } = request.services();

    return await userService.create(request.payload);
  }
};