'use strict';

const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/user/login',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
          payload: Joi.object({
            password: Joi.string().required().min(8).description('Password of the user, at least 8 characters'),
            mail: Joi.string().required().email().max(256).example('alexis.gousseau@gmail.com').description('Email of the user'),
          })
        }
    },
    handler: async (request) => {

        const { userService } = request.services();

        return await userService.login(request.payload);
    }
};