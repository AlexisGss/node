'use strict';

const Joi = require('joi')

module.exports = {
  method: 'patch',
  path: '/user/patch',
  options: {
      auth : {
          scope: [ 'admin' ]
      },
      tags: ['api'],
      validate: {
        payload: Joi.object({
          id: Joi.number().integer().min(1).description("Id of the user"),
          firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
          lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
          username: Joi.string().required().min(3).example('JDoe').description('Username of the user'),
          password: Joi.string().required().min(8).description('Password of the user, at least 8 characters'),
          mail: Joi.string().required().email().max(256).example('jdoe@gmail.com').description('Email of the user'),
          scope: Joi.string().required().example('user').description('Role of the user'),
        })
      }
  },
  handler: async (request) => {

      const { userService } = request.services();

      return await userService.update(request.payload);
  }
};