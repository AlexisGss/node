'use strict';

module.exports = {
    method: 'get',
    path: '/getUser',
    options: {
        auth : {
            scope: [ 'user', 'admin' ]
        },
        tags: ['api']
    },
    handler: async (request, h) => {
    
        const { userService } = request.services();
        
        return await userService.list('user');
    }
};